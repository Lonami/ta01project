package dev.lonami.calnex.model;

import androidx.annotation.NonNull;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import dev.lonami.calnex.DailyTask;
import dev.lonami.calnex.TimeRange;

public class Subject {
    private String name;
    private String shortname;
    private int year;
    private int semester;
    private List<Teacher> teachers;
    private String url;
    private List<Time> times;

    public Subject(JSONObject json, HashMap<String, Teacher> teachers, HashMap<String, Room> rooms) throws JSONException {
        this.name = json.getString("name");
        this.shortname = json.getString("shortname");
        this.year = json.getInt("year");
        this.semester = json.getInt("semester");

        this.teachers = new ArrayList<>();
        JSONArray t = json.getJSONArray("teachers");
        for (int i = 0; i < t.length(); ++i) {
            this.teachers.add(teachers.get(t.getString(i)));
        }

        this.url = json.getString("url");

        this.times = new ArrayList<>();
        JSONArray times = json.getJSONArray("times");
        for (int i = 0; i < t.length(); ++i) {
            this.times.add(new Time(times.getJSONObject(i), rooms));
        }
    }

    @NonNull
    @Override
    public String toString() {
        return String.format(Locale.ENGLISH, "[Year %d / Semester %d] [%s] %s (%s)",
                this.year, this.semester, this.shortname, this.name, this.url);
    }

    public String getName() {
        return name;
    }

    // TODO This is for testing purposes and of course we can't convert a subject to a task on a specific day like this
    public DailyTask asDailyTask() {
        return new DailyTask(shortname, name, new TimeRange(11, 30, 13, 30), "Sun", teachers.get(0).getName());
    }
}
