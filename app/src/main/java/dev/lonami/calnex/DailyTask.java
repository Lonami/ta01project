package dev.lonami.calnex;

import android.os.Parcel;
import android.os.Parcelable;

public class DailyTask implements Parcelable {
    public final String name;
    public final String fullname;
    public final TimeRange timespan;
    public final String room;
    public final String lecturer;

    public DailyTask(String name, String fullname, TimeRange timespan, String room, String lecturer) {
        this.name = name;
        this.fullname = fullname;
        this.timespan = timespan;
        this.room = room;
        this.lecturer = lecturer;
    }

    private DailyTask(Parcel in) {
        this.name = in.readString();
        this.fullname = in.readString();
        this.timespan = in.readParcelable(TimeRange.class.getClassLoader());
        this.room = in.readString();
        this.lecturer = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(fullname);
        dest.writeParcelable(timespan, flags);
        dest.writeString(room);
        dest.writeString(lecturer);
    }

    public static final Parcelable.Creator<DailyTask> CREATOR = new Parcelable.Creator<DailyTask>() {
        public DailyTask createFromParcel(Parcel in) {
            return new DailyTask(in);
        }

        public DailyTask[] newArray(int size) {
            return new DailyTask[size];
        }
    };
}
